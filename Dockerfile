FROM gcr.io/distroless/java:8

COPY /target/scs-globalktable-lookup-joins*.jar app.jar
CMD ["/app.jar"]
