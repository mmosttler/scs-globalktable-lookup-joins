package com.mm.scs.test;

import java.util.function.BiFunction;

import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PublishAvailProductChangesApplication 
{
	public static void main(String[] args) 
	{
		SpringApplication.run(PublishAvailProductChangesApplication.class, args);
	}

	/**
	 * @param KStream<String, String> = <skuPartNumber, availabilityRecord>
	 * @param GlobalKTable<String, String> = <skuPartNumber, baseCode>
	 * @return KStream<String, String> = <baseCode, changeLogRecord>
	 */
	@Bean
	public BiFunction<KStream<String, String>, GlobalKTable<String, String>, KStream<String, String>> processAvailabilityChange()
	{
	    return (availStream, skuBaseCodeLookup) -> availStream
	        .join(skuBaseCodeLookup, (leftKey, leftValue) -> leftKey, JoinAvailability.TO_SKU_BASECODE)
	        .filter((skuPartNumber, chgLogJoiner) -> chgLogJoiner.isPresent())
	        .map((skuPartNumber, chgLogJoiner) -> chgLogJoiner.get())
	        ;
	}

	/**
     * @param KStream<String, String> = <skuPartNumber, baseCode>
     * @param GlobalKTable<String, String> = <skuPartNumber, availabilityRecord>
     * @return KStream<String, String> = <baseCode, changeLogRecord>
	 */
	@Bean
    public BiFunction<KStream<String, String>, GlobalKTable<String, String>, KStream<String, String>> processNewSkuAvailability()
    {
        return (skuBaseCodeLookup, availStream) -> skuBaseCodeLookup
            .join(availStream, (leftKey, leftValue) -> leftKey, JoinAvailability.TO_AVAILABILITY)
            .filter((skuPartNumber, chgLogJoiner) -> chgLogJoiner.isPresent())
            .map((skuPartNumber, chgLogJoiner) -> chgLogJoiner.get())
            ;
    }
}