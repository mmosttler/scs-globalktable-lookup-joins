package com.mm.scs.test;

/**
 */
public class AvailabilityRecord
{
    private String skuPartNumber = null;
    private boolean isAvailable;

    private AvailabilityRecord(String inPartNumber, boolean isAvail)
    {
        skuPartNumber = inPartNumber;
        isAvailable = isAvail;
    }
    
    public final String getSkuPartNumber()
    {
        return skuPartNumber;
    }
    
    public final boolean isAvailable()
    {
        return isAvailable;
    }
    
    public static Builder newBuilder()
    {
        return new Builder();
    }    
    
    public static class Builder
    {
        private String partNumber = null;
        private boolean isAvailable;
        
        public Builder from(String availArray)
        {
            partNumber = availArray;
            isAvailable = true;
            return this;
        }
        
        public AvailabilityRecord build()
        {
            return new AvailabilityRecord(partNumber, isAvailable);
        }
    }
}
