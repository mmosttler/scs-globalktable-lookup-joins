package com.mm.scs.test;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.ValueJoiner;

/**
 */
public class JoinAvailability
{
    public static final ValueJoiner<String, String, Optional<KeyValue<String, String>>> TO_SKU_BASECODE = new AvailabilitySkuJoiner()
        {
            @Override
            public Optional<KeyValue<String, String>> apply(String availRecord, String baseCodeRecord)
            {
                return join(availRecord, baseCodeRecord);
            }        
        };

    public static final ValueJoiner<String, String, Optional<KeyValue<String, String>>> TO_AVAILABILITY = new AvailabilitySkuJoiner()
        {
            @Override
            public Optional<KeyValue<String, String>> apply(String baseCodeRecord, String availRecord)
            {
                return join(availRecord, baseCodeRecord);
            }
        };
    
    private JoinAvailability()
    {
        //
    }

    private abstract static class AvailabilitySkuJoiner implements ValueJoiner<String, String, Optional<KeyValue<String, String>>>
    {
        protected Optional<KeyValue<String, String>> join(String availRecord, String baseCodeRecord)
        {
            return Optional.ofNullable(baseCodeRecord)
                .filter(StringUtils::isNoneBlank)
                .map(baseCode -> new KeyValue<>(baseCode, getChangeLogRecord(baseCode, availRecord)) )
                .filter(kv -> kv.value.isPresent())
                .map(kv -> new KeyValue<>(kv.key, kv.value.get()))
                .filter(kv -> StringUtils.isNoneBlank(kv.value));        
        }
        
        private Optional<String> getChangeLogRecord(String baseCode, String availRecord)
        {
            String clr= null;
            
            Optional<AvailabilityRecord> availRec = Optional.ofNullable(
                AvailabilityRecord.newBuilder()
                    .from(availRecord)
                    .build())
                .filter(rec -> StringUtils.isNoneBlank(rec.getSkuPartNumber()));
    
            if(availRec.isPresent())
            {
                AvailabilityRecord avail = availRec.get();
                clr = baseCode+"."+avail.getSkuPartNumber()+"="+avail.isAvailable();
            }
            return Optional.ofNullable(clr);
        }
    }
}
