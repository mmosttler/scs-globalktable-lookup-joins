package com.mm.scs.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.mm.scs.test.PublishAvailProductChangesApplication;

@ExtendWith(MockitoExtension.class)
abstract class AbstractAvailabilityChanglogTestCase 
{
//	private static final String SCHEMA_REGISTRY_SCOPE = AbstractAvailabilityChanglogTestCase.class.getName();
//	private static final String MOCK_SCHEMA_REGISTRY_URL = "mock://" + SCHEMA_REGISTRY_SCOPE;
		
	protected static final String INPUT_AVAIL_TOPIC = "availability";
	protected static final String INPUT_LOOKUP_TOPIC = "lookup";
	protected static final String OUTPUT_CHGLOG_TOPIC = "changelog";

	protected static final Serde<String> stringSerde = Serdes.String();
    
    private static Properties streamsConfiguration;

    private TopologyTestDriver testDriver;
    protected TestInputTopic<String, String> inputAvailabilityTopic;
    protected TestInputTopic<String, String> inputLookupTopic;
    protected TestOutputTopic<String, String> outputChgLogTopic;

	@InjectMocks
	protected PublishAvailProductChangesApplication app;

	protected final String baseCode = "31979X";
	
	@BeforeAll
	public static void intialize()
	{
        // Need to be set even these do not matter with TopologyTestDriver
		streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "TopologyTestDriver");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234");
        streamsConfiguration.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, (10 * 1024 * 1024L));
//        streamsConfiguration.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, MOCK_SCHEMA_REGISTRY_URL);
	}
	
    /**
     * Setup Stream topology
     * Add KStream based on @StreamListener annotation
     * Add to(topic) based @SendTo annotation
     */
	@BeforeEach
	public void setup()
	{	
	    //Create Actual Stream Processing pipeline
	    final StreamsBuilder builder = new StreamsBuilder();
	    setTopology(builder);

	    testDriver = new TopologyTestDriver(builder.build(), streamsConfiguration);
	    inputAvailabilityTopic = testDriver.createInputTopic(INPUT_AVAIL_TOPIC, stringSerde.serializer(), stringSerde.serializer());
	    inputLookupTopic = testDriver.createInputTopic(INPUT_LOOKUP_TOPIC, stringSerde.serializer(), stringSerde.serializer());
	    outputChgLogTopic = testDriver.createOutputTopic(OUTPUT_CHGLOG_TOPIC, stringSerde.deserializer(), stringSerde.deserializer());
	}	
	
	protected abstract void setTopology(StreamsBuilder builder);

	@AfterEach
    public void tearDown() 
	{
        try 
        {
            testDriver.close();
//            MockSchemaRegistry.dropScope(SCHEMA_REGISTRY_SCOPE);
        } 
        catch (final RuntimeException e) 
        {   // https://issues.apache.org/jira/browse/KAFKA-6647 causes exception when executed in Windows, ignoring it. Logged stacktrace cannot be avoided
            System.out.println("Ignoring exception, test failing in Windows due this exception:" + e.getLocalizedMessage());
        }
    }	

	protected final KeyValue<String, String> getExpectedKV(String skuPartNumber, boolean avail)
	{
	    return new KeyValue<>(baseCode, skuPartNumber + "=" + avail);
	}
	
	protected String getAvailabilityRecord(String partNumber, boolean avail) throws JSONException
	{
        JSONObject item = new JSONObject();
        item.put("partNumber", partNumber);
        item.put("skuId", StringUtils.EMPTY);
        item.put("itemId", StringUtils.EMPTY);
        item.put("brandId", StringUtils.EMPTY);
        item.put("shipdate", "1L");
        item.put("availabilityIndicator", Boolean.toString(avail));
        item.put("isAvailable", avail);

        JSONArray availabilityFeedArray = new JSONArray();
        availabilityFeedArray.put(item);
        JSONObject message = new JSONObject();
        message.put("availabilityFeed", availabilityFeedArray);
        
	    return message.toString();
	}
	
}
