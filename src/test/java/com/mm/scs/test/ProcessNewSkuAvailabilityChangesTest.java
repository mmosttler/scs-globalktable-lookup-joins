package com.mm.scs.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.json.JSONException;
import org.junit.jupiter.api.Test;

class ProcessNewSkuAvailabilityChangesTest extends AbstractAvailabilityChanglogTestCase
{
    /**
     * @see com.mm.scs.test.AbstractAvailabilityChanglogTestCase#setTopology(org.apache.kafka.streams.StreamsBuilder)
     */
    @Override
    protected void setTopology(StreamsBuilder builder)
    {
        GlobalKTable<String, String> inputAvail = builder.globalTable(INPUT_AVAIL_TOPIC, Consumed.with(stringSerde, stringSerde));
        KStream<String, String> inputLookup = builder.stream(INPUT_LOOKUP_TOPIC, Consumed.with(stringSerde, stringSerde));
        
        //Add the stream processor.
        final KStream<String, String> output = app.processNewSkuAvailability().apply(inputLookup, inputAvail);
        
        output.to(OUTPUT_CHGLOG_TOPIC, Produced.with(stringSerde, stringSerde));
    }
	
    @Test
    public void processNewProductSkuWithExistingAvail_publishChgLog() throws JSONException
    {
        final String skuPartNumber = "1234Z";
        final KeyValue<String, String> expected = getExpectedKV(skuPartNumber, true);
        
        //Add availability change
        inputAvailabilityTopic.pipeInput(skuPartNumber, getAvailabilityRecord(skuPartNumber, true));

        //Add sku to base code lookup record that won't match
        inputLookupTopic.pipeInput(skuPartNumber, baseCode);

        //Validate results
        assertEquals(expected, outputChgLogTopic.readKeyValue());
        //should be no more records in the output topic
        assertTrue(outputChgLogTopic.isEmpty(), outputChgLogTopic.getQueueSize() + " remaining records");                   
    }

    @Test
    public void processNewProductSkuWithNoExistingAvail_noChgLogPublished() throws JSONException
    {
        final String skuPartNumber = "1234Z";
        
        //Add sku to base code lookup record that won't match
        inputLookupTopic.pipeInput("09876A", baseCode);
        
        //Add availability change
        inputAvailabilityTopic.pipeInput(skuPartNumber, getAvailabilityRecord(skuPartNumber, true));

        //should be no more records in the output topic
        assertTrue(outputChgLogTopic.isEmpty(), outputChgLogTopic.getQueueSize() + " remaining records");                   
    }
}
