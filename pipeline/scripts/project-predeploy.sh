# Set values in secret config prior to applying during deploy.
echo "Pre Deploying Script Running"

KAFKA_API_KEY=`echo -n $KAFKA_API_KEY | base64 -w 0`
KAFKA_API_SECRET=`echo -n $KAFKA_API_SECRET | base64 -w 0`
KAFKA_SCHEMA_REGISTRY_API_KEY=`echo -n $KAFKA_SCHEMA_REGISTRY_API_KEY | base64 -w 0`
KAFKA_SCHEMA_REGISTRY_API_SECRET=`echo -n $KAFKA_SCHEMA_REGISTRY_API_SECRET | base64 -w 0`

sed -i "s|__KAFKA_API_KEY__|${KAFKA_API_KEY}|g" kube/secrets.yaml
sed -i "s|__KAFKA_API_SECRET__|${KAFKA_API_SECRET}|g" kube/secrets.yaml
sed -i "s|__KAFKA_SCHEMA_REGISTRY_API_KEY__|${KAFKA_SCHEMA_REGISTRY_API_KEY}|g" kube/secrets.yaml
sed -i "s|__KAFKA_SCHEMA_REGISTRY_API_SECRET__|${KAFKA_SCHEMA_REGISTRY_API_SECRET}|g" kube/secrets.yaml
